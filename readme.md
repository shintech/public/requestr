# shintech/public/requestr

## Table of Contents
1. [ Synopsis ](#synopsis)
2. [ Installation/Configuration ](#install)
3. [ Usage ](#usage)

<a name="synopsis"></a>
### Synopsis

Node Request package
  
<a name="install"></a>
### Installation

    npm install requestr

<a name="usage"></a>
### Usage
