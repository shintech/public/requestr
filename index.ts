import { http, https } from 'follow-redirects';
import { IncomingHttpHeaders, OutgoingHttpHeaders } from 'http';
import { Socket } from 'net';
import { safeStringify } from './lib/utils';

export type AllowedMethods = 'GET' | 'POST' | 'PUT' | 'DELETE'

export interface RequestOptions {
  headers?: OutgoingHttpHeaders | undefined;
  auth?: string | null | undefined;
  body?: Record<string, any> | string;
  secure?: boolean | undefined;
}

export interface HttpResponse {
  statusCode?: number | undefined;
  statusMessage?: string | undefined;
  method?: string | undefined;
  url?: string | undefined;
  headers: IncomingHttpHeaders;
  rawHeaders: string[];
  socket: Socket;
  readonly searchParams: URLSearchParams;
  body?: Record<string, any>;
}

/**
  * Makes an http request.
  * @param {AllowedMethods} method A string specifying the HTTP request method.
  * @param {String} url The absolute input URL to parse.
  * @param {OutgoingHttpHeaders} options.headers An object containing request headers.
  * @param {Record<string, any> | string} options.body The data to write to the body of the request .
  * @param {string | null | undefined} options.auth Basic authentication ('user:password') to compute an Authorization header.
  * @param {boolean | undefined} options.secure Make a secure request if set to true.
  *
  * @typedef {Object} HttpResponse
  * @property {number|undefined} statusCode The 3-digit HTTP response status code. E.G. `404`.
  * @property {string|undefined} statusMessage The HTTP response status message (reason phrase). E.G. `OK` or `Internal Server Error`.\
  * @property {string|undefined} method The request method as a string. Read only. Examples: `'GET'`, `'DELETE'`.
  *
  * @property {string|undefined} url Request URL string. This contains only the URL that is present in the actual HTTP request.
  * @property {IncomingHttpHeaders} headers Key-value pairs of header names and values. Header names are lower-cased.
  * @property {string[]} rawHeaders The keys and values are in the same list. It is _not_ a list of tuples. So, the even-numbered offsets are key values, and the * odd-numbered offsets are the associated values.
  * @property {Socket} socket The `net.Socket` object associated with the connection.
  * @property {URLSearchParams} searchParams The URLSearchParams object representing the query parameters of the URL. This property is read-only but the URLSearchParams object it provides can be used to mutate the URL instance.
  * @property {Record<string, any>} body The response body.
  *
  * @returns {Promise<HttpResponse>} Promise\<HttpResponse\>
*/

const request: (
  method: AllowedMethods,
  url: string,
  options?: RequestOptions
) => Promise<HttpResponse> = (
  method,
  url,
  options
) => new Promise((resolve, reject) => {
  const { hostname, pathname, port, protocol, search, searchParams } = new URL(url);

  const headers = options?.headers || {
    'Content-Type': 'application/json'
  };

  const chunks: Uint8Array[] = [];

  const lib = options?.secure || protocol === 'https' ? https : http;

  const req = lib.request({
    protocol,
    hostname,
    port,
    path: pathname + search,
    method,
    headers,
    auth: options?.auth
  }, res => {
    res.on('data', chunk => chunks.push(chunk));

    res.on('end', () => {
      const statusCode = res.statusCode;
      const statusMessage = res.statusMessage;
      const method = res.method;
      const url = res.url;
      const headers = res.headers;
      const rawHeaders = res.rawHeaders;
      const socket = res.socket;

      const body: Record<string, any> = (headers['content-type'] === 'application/json')
        ? JSON.parse(Buffer.concat(chunks).toString())
        : Buffer.concat(chunks).toString();

      resolve({
        statusCode,
        statusMessage,
        method,
        url,
        headers,
        rawHeaders,
        socket,
        searchParams,
        body
      });
    });
  });

  req.on('error', reject);

  if (options?.body) {
    if (headers['Content-Type'] !== 'application/json') {
      req.write(options.body);
    } else {
      req.write(safeStringify(options?.body));
    }
  }

  req.end();
});

export default request;
