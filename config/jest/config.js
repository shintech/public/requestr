/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */

const ignorePatterns = [
  '<rootDir>/node_modules/',
  '<rootDir>/archive'
];

const setupFiles = [
  '<rootDir>/config/jest/setup.js'
];

module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testPathIgnorePatterns: ignorePatterns,
  watchPathIgnorePatterns: ignorePatterns,
  rootDir: process.cwd(),
  setupFiles,
  testRegex: 'test\\.ts$'
};
